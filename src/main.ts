import { NestFactory } from '@nestjs/core';

import { AppModule } from './app.module';

async function bootstrap() {
  const port = process.env.APP_PORT;

  if (port === undefined) {
    throw new Error('You must set the APP_PORT environment variable to start the appclication.');
  }

  const app = await NestFactory.create(AppModule);
  await app.listen(port);
}

bootstrap();
